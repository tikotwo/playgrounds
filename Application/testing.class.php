<?php if (!defined('ONPATH')) exit('No direct script access allowed'); //Mencegah akses langsung ke class


class testing extends core{


	function testing()
	{
		parent::Core();

		$this->LoadModule("Testings");
		
        $this->LoadModule("Member");
        $this->LoadModule("Memberoptions");
        $this->LoadModule("Products");
        $this->LoadModule("Options");
        $this->LoadModule("Shipping");



		$this->LoadModule("Paging");
		$this->Module->Paging->setPaging(16, 5);

		//Load Session
		$this->LoadModule("Session");
		$this->Module->Session->refresh();
		$getSession = $this->Module->Session->getSessionHistory(session_id());
		$this->Template->assign("theUsername", $getSession['vUsername']);
		if ($getSession['vUsername'] != "") {
			$this->mUsername = $getSession['vUsername'];
			define('_SESSION_NOW', $this->mUsername);
			$this->detailMember = $this->Module->Member->detailMember(NULL, $this->mUsername);
			$this->Template->assign("detailMember", $this->detailMember);
			$this->Module->Session->updateSession($session_Signature, $this->mUsername);
			//Last Login
			$this->Template->assign("lastLogin", $this->Module->Member->lastLogin($this->mUsername));
			//Count Unread Messages
			$this->Template->assign("unreadMessage", $this->Module->Member->countUnreadMessage($this->mUsername));
		}

		//Load General Process
		include 'inc/general.php';

		$this->Template->assign("dirProducts", $this->Config['upload']['products']);
		$this->Template->assign("SIGNATURE", "testing");
	}

	//Category Products
	function main()
	{



		

		// Pagnation
		$thePage = $_GET['page'];
		$this->Template->assign("Page" , $thePage);
		$title_Page = ($thePage == "") ? "" : " Halaman " . $thePage . " - ";
		$this->Module->Paging->selected = " class=\"current\"";
		$this->Module->Paging->formatLink = "<li><a href=\"#URL\"#SELECTED>#TITLE</a></li>";
		$this->Module->Paging->URL = $this->getURL() . "testing/";
		$Show_ = ($_GET['show'] == "") ? "grid" : $_GET['show'];
		$this->Template->assign("Show", $Show_);

		$Sort_ = $_GET['sort']; //($_GET['sort']=="")?"new":$_GET['sort'];
		$this->Template->assign("Sort", $Sort_);


		$this->Module->Paging->query("SELECT * FROM cpproductitem");
	

		$baca = $this->Module->Testings->getAllProduct();
		$i = 0;

		$listBrand = $this->Module->Products->listBrand("all", "1");
		
		$page = $this->Module->Paging->print_info();
		$this->Template->assign("status", "Product: " . $page[start] . " - " . $page[end] . " of " . $page[total]); //." [Total ".$page[total_pages]." Product]");
	

		// while($Baca = $this->Db->sql_array($baca)){

		// 	$getHarga = $this->Module->Products->getAdminUserPrice($Baca['iPrice'] , $Baca['iPercent'] , "value" , $Baca['id']);

		// 	$Data[$i] = array(
		// 		'No' => ($i + 1),
		// 		'Item' => array_merge($Baca , $getHarga),
		// 	);	

		// 	$i++;

		// }	

		while($Baca = $this->Module->Paging->result_assoc()){
			$getHarga = $this->Module->Products->getAdminUserPrice($Baca['iPrice'] , $Baca['iPercent'] , "value" , $Baca['id']);
				$Data[$i] = array(
				'No' => ($i + 1),
				'Item' => array_merge($Baca , $getHarga),
				'no_html' => strip_tags($Baca['tDetail']),
			);	
			$i++;
		}
		
		$this->Template->assign("link" , $this->Module->Paging->print_link());
		$this->Template->assign('listBarang' , $Data);
		$this->Template->assign("brandList", $listBrand );

		echo $this->Template->show("testing.html");

	
	}
	
	function sendemail(){
		echo $this->Template->show("view_send_email.html");
	}

	function storeemail(){

		$to = $_POST['email'];
		$subject = $_POST['subject'];
		// $check = $_POST['check'];

		$bodyEmail = $_POST['bodyEmail'];
		
		
		$url = 'https://api.elasticemail.com/v2/email/send';

		try{
				$post = array('from' => 'ikoma.co.id@gmail.com',
				'fromName' => 'PT. Ikoma',
				'apikey' => 'ea1f0caa-db8b-4397-957d-ffb879a43f07',
				'subject' => $subject,
				'to' => $to,
				'bodyHtml' => $bodyEmail,
				'isTransactional' => false);
				
				$ch = curl_init();
				curl_setopt_array($ch, array(
					CURLOPT_URL => $url,
					CURLOPT_POST => true,
					CURLOPT_POSTFIELDS => $post,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_HEADER => false,
					CURLOPT_SSL_VERIFYPEER => false
				));
				
				$result=curl_exec ($ch);
				curl_close ($ch);
				
				$data =  json_decode($result);

				
				if($data->success){
					$this->Template->assign("msg" , "ok" );
					echo $this->Template->show("view_send_email.html");
				}else{
					$this->Template->assign("msg" , "gagal" );
					echo $this->Template->show("view_send_email.html");
				}
		}
		catch(Exception $ex){
			echo $ex->getMessage();
		}
        
		
	}
    


}