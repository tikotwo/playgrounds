<?php if (!defined('ONPATH')) exit('No direct script access allowed'); //Mencegah akses langsung ke class

class memberproduk extends Core
{
	var $mUsername, $detailMember;

	function memberproduk()
	{
		parent::Core();

		$this->LoadModule("Member");

		$this->LoadModule("Paging");
		$this->Module->Paging->setPaging(5, 5);

		$this->LoadModule("Tanggal");
		$this->LoadModule("Options");
		$this->LoadModule("Memberoptions");
		$this->LoadModule("Emailoptions");
		$this->LoadModule("Shipping");
		$this->LoadModule("Products");
		$this->LoadModule("Tools");

		//Load General Process
		include 'inc/general.php';

		include 'inc/common.php';
		$this->Template->assign("Signature_page", $this->uri(2));
		$this->Template->assign("Signature_Sub", $this->uri(2));
		$this->Template->assign("dirPicture", $this->Config['member']['image']);
	}

	//Main
	function main()
	{
		echo $this->Template->Show("main_account.html");
	}

	//Reward Poin
	function reward()
	{
		$this->checkAuthMember(array("all", "all"), $this->detailMember); //Check Auth
		$this->Template->assign("Signature_Sub", "reward");

		$this->Pile->fileDestination = $this->Config['upload']['products'];
		$this->Template->assign("dirProducts", $this->Config['upload']['products']);


		//if submit process do-------------------------------
		$Submit = ($_POST['submit']) ? $_POST['submit'] : $_GET['submit'];
		$Id = ($_POST['id']) ? $_POST['id'] : $_GET['id'];

		$Do = $_GET['do'];
		$this->Template->assign("Do", $Do);

		$Show = $_GET['show'];
		$this->Template->assign("Show", $Show);

		$vCompare = trim($_GET['vCompare']);
		$this->Template->assign("vCompare", $vCompare);

		$vTeks = $this->Db->string_only(trim($_POST['vTeks']));
		$this->Template->assign("vTeks", $vTeks);

		$this->Template->assign("Page", $_GET['page']);

		$idCat = $_GET['cat'];
		$this->Template->assign("idCat", $idCat);

		if ($Submit) {
			//get the ction
			$Action = ($_POST['action']) ? $_POST['action'] : $_GET['action'];

			switch ($Action) {
					//Add Update Item
				case "rendeem":
					$DetailPoin = $this->Module->Products->detailPoin($Id);
					if ($DetailPoin['id'] != "") {
						$Progress = FALSE;

						if ($DetailPoin['iType'] == "1") {
							if ($this->detailMember['iPoinAff'] >= $DetailPoin['dPoin']) {
								$Progress = TRUE;
								$typePoin = "aff";
							} else
								$this->Template->reportMessage("error", "Ops! Nilai poin affiliasi anda tidak mencukupi");
						}

						if ($DetailPoin['iType'] == "0") {
							if ($this->detailMember['iPoinBuy'] >= $DetailPoin['dPoin']) {
								$Progress = TRUE;
								$typePoin = "buy";
							} else
								$this->Template->reportMessage("error", "Ops! Nilai poin pembelian anda tidak mencukupi");
						}

						if ($Progress == TRUE) {
							$idPoinItem = $DetailPoin['id'];
							$dOrder = date("Y-m-d H:i:s");
							$dPoin = $DetailPoin['dPoin'];
							$vStatus = "progress";
							$vIP = $_SERVER['REMOTE_ADDR'];

							if ($this->Module->Products->addRendeem_Poin(array($this->mUsername, $idPoinItem, $dOrder, $dPoin, $vStatus, $vIP))) {
								$this->Module->Member->potongPoin($this->mUsername, $dPoin, $typePoin);
								$detail_Member = $this->Module->Member->detailMember(NULL, $this->mUsername);
								$this->Template->assign("detailMember", $detail_Member);

								//Kirim Email Notifikasi
								$EmailDetail = array(
									'#name#' => stripslashes($detail_Member['vName']),
									'#item#' => $DetailPoin['vName'],
									'#poin#' => $dPoin
								);

								//Kirim log kepada admin
								$this->Module->Tools->kirimEmail($this->Config['default']['email'], $EmailDetail, "order_poin");

								//Send Notification
								$this->addNotif($this->Config['admin']['user'], "new_rendeem", " (" . $detail_Member['vName'] . ")", "?do=detailorder&id=" . $DetailPoin['id'] . "&notif=read");
								//----------------------------								

								$this->Template->reportMessage("success", "Terima kasih, poin anda sudah di rendeem dan akan segera kami proses");
							}
						}
					} else
						$this->Template->reportMessage("error", "Maaf! Produk yang ingin anda rendeem tidak tersedia");

					break;
					//Delete Item
				case "deleterendeem":
					$DetailRendeem = $this->Module->Products->detailRendeem_Poin($Id, $this->mUsername);
					if ($DetailRendeem['vStatus'] == "progress") {
						$DetailPoin = $this->Module->Products->detailPoin($DetailRendeem['idPoinItem']);
						$typePoin = ($DetailPoin['iType'] == "1") ? "aff" : "buy";
						$this->Module->Member->tambahPoin($this->mUsername, $DetailRendeem['dPoin'], $typePoin);
						$this->Template->assign("detailMember", $this->Module->Member->detailMember(NULL, $this->mUsername));

						if ($this->Module->Products->deleteRendeem_Poin($Id, $this->mUsername))
							$this->Template->reportMessage("success", "Rendeem poin sudah di batalkan");
					} else
						$this->Template->reportMessage("error", "Rendeem poin ini tidak dapat di batalkan karena sudah di proses oleh admin");
					break;
			}
		}

		switch ($Do) {
				//Detail Produk
			case "confirm":
			case "detail":
				$detailItem = $this->Module->Products->detailPoin($Id);
				$this->Template->assign("detailItem", $detailItem);
				$this->Template->assign("detailCategory", $this->Module->Products->getCategoryPoin($detailItem['idCategory']));

				switch ($Do) {
					case "confirm":
						echo $this->Template->show("main_poin_confirm.html");
						break;
					default:
						echo $this->Template->show("main_poin_detail.html");
						break;
				}
				break;
			case "rendeem":
				$this->Template->assign("Signature_Sub", "rendeem");
				if (($vCompare == "") or ($vTeks == ""))
					$this->Module->Paging->query("SELECT * FROM cppoinorder WHERE vUsername='" . $this->mUsername . "' ORDER BY id DESC");
				else
					$this->Module->Paging->query("SELECT * FROM cppoinorder WHERE vUsername='" . $this->mUsername . "' AND " . $vCompare . " LIKE '%" . $vTeks . "%' ORDER BY id DESC");

				$this->Module->Paging->URL = $this->getURL() . "memberproduk/reward";
				$this->Module->Paging->dataLink = "do=rendeem&vCompare=" . $vCompare . "&vTeks=" . $vTeks;
				$page = $this->Module->Paging->print_info();
				$this->Template->assign("status", "Rendeem Poin " . $page[start] . " - " . $page[end] . " of " . $page[total] . " [Total " . $page[total_pages] . " Groups]");
				$i = 0;
				while ($Baca = $this->Module->Paging->result_assoc()) {
					$Data[$i] = array('No' => ($i + 1),	'Item' => $Baca, 'Detail' => $this->Module->Products->detailPoin($Baca['idPoinItem']));
					$i++;
				}

				$this->Template->assign("list", $Data);
				$this->Template->assign("link", $this->Module->Paging->print_link());

				echo $this->Template->show("main_poin_rendeem.html");
				break;
			default:
				$detailCat = $this->Module->Products->detailCategoryPoin($idCat);
				if ($detailCat['id'] == "") {
					$this->Template->assign("listCategory", $this->Module->Products->listCategoryPoin());
					echo $this->Template->show("main_poin_cat.html");
				} else {
					$this->Template->assign("listCategory", $this->Module->Products->listCategoryPoin());
					$this->Template->assign("detailCat", $detailCat);

					if (($vCompare == "") or ($vTeks == ""))
						$this->Module->Paging->query("SELECT * FROM cppoinitem WHERE idCategory='" . $detailCat['id'] . "' ORDER BY id DESC");
					else
						$this->Module->Paging->query("SELECT * FROM cppoinitem WHERE idCategory='" . $detailCat['id'] . "' AND " . $vCompare . " LIKE '%" . $vTeks . "%' ORDER BY id DESC");

					$this->Module->Paging->URL = $this->getURL() . "memberproduk/reward";
					$this->Module->Paging->dataLink = "cat=" . $detailCat['id'] . "&vCompare=" . $vCompare . "&vTeks=" . $vTeks;
					$page = $this->Module->Paging->print_info();
					$this->Template->assign("status", "Poin Produk " . $page[start] . " - " . $page[end] . " of " . $page[total] . " [Total " . $page[total_pages] . " Groups]");
					$i = 0;
					while ($Baca = $this->Module->Paging->result_assoc()) {
						$Data[$i] = array('No' => ($i + 1),	'Item' => $Baca);
						$i++;
					}

					$this->Template->assign("list", $Data);
					$this->Template->assign("link", $this->Module->Paging->print_link());

					echo $this->Template->show("main_poin_index.html");
				}
				break;
		}
	}

	//Wishlist
	function wishlist()
	{
		$this->checkAuthMember(array("all", "all"), $this->detailMember); //Check Auth
		$this->Template->assign("dirProducts", $this->Config['upload']['products']);
		$this->Template->assign("Signature_Sub", "wishlist");

		//if submit process do-------------------------------
		$Submit = ($_POST['submit']) ? $_POST['submit'] : $_GET['submit'];
		$Id = ($_POST['id']) ? $_POST['id'] : $_GET['id'];

		if ($Submit) {
			//get the ction
			$Action = ($_POST['action']) ? $_POST['action'] : $_GET['action'];

			switch ($Action) {
				case "delete":
					if ($Id != "") {
						$this->Module->Products->deleteWishlist($Id, $this->mUsername);
						$this->Template->reportMessage("success", "Produk sudah di hapus dari daftar wishlist anda");
					}
					break;
			}
		}

		//----------------------------------------------------------
		$Do = $_GET['do'];
		$this->Template->assign("Do", $Do);

		$Show = $_GET['show'];
		$this->Template->assign("Show", $Show);

		$vCompare = trim($_GET['vCompare']);
		$this->Template->assign("vCompare", $vCompare);

		$vTeks = $this->Db->string_only(trim($_POST['vTeks']));
		$this->Template->assign("vTeks", $vTeks);

		$this->Template->assign("Page", $_GET['page']);
		//----------------------------------------------------------

		switch ($Do) {
			default:

				$this->Module->Paging->query("SELECT * FROM cpwishlist WHERE vUsername='" . $this->mUsername . "' ORDER BY id DESC");
				$this->Module->Paging->URL = $this->getURL() . "memberproduk/wishlist";
				$this->Module->Paging->dataLink = "";
				$page = $this->Module->Paging->print_info();
				$this->Template->assign("status", "Wishlist " . $page[start] . " - " . $page[end] . " of " . $page[total] . " [Total " . $page[total_pages] . " Groups]");
				$i = 0;
				while ($Baca = $this->Module->Paging->result_assoc()) {
					$detailItem = $this->Module->Products->detailItem($Baca['idProduct'], "1");
					$getHarga = $this->Module->Products->getAdminUserPrice($detailItem['iPrice'], $detailItem['iPercent'], "value", $detailItem['id']);
					if ($detailItem['id'] != "") {
						$Data[$i] = array('No' => ($i + 1), 'Wishlist' => $Baca, 'Item' => array_merge($detailItem, $getHarga));
						$i++;
					}
				}

				$this->Template->assign("list", $Data);
				$this->Template->assign("link", $this->Module->Paging->print_link());

				echo $this->Template->show("main_wishlist.html");
				break;
		}
	}

	//Transaksi Pembeli
	function transaksipembeli() {
		$this->checkAuthMember(array("all", "all"), $this->detailMember); //Check Auth
		$this->Template->assign("Signature_Sub", "order");

		//if submit process do-------------------------------
		$Submit = ($_POST['submit']) ? $_POST['submit'] : $_GET['submit'];
		$Id = ($_POST['id']) ? $_POST['id'] : $_GET['id'];

		if ($Submit) {
			//get the ction
			$Action = ($_POST['action']) ? $_POST['action'] : $_GET['action'];

			switch ($Action) {
				case "action":
					break;
			}
		}

		//----------------------------------------------------------
		$Do = $_GET['do'];
		$this->Template->assign("Do", $Do);

		$Show = $_GET['show'];
		$this->Template->assign("Show", $Show);

		$vCompare = trim($_GET['vCompare']);
		$this->Template->assign("vCompare", $vCompare);

		$vTeks = $this->Db->string_only(trim($_POST['vTeks']));
		$this->Template->assign("vTeks", $vTeks);
		$this->Template->assign("Page", $_GET['page']);

		$this->Template->assign("totalOrder", $this->Module->Products->countOrder("", $this->mUsername));

		switch ($Do) {
			case "print":
				if ($_GET['nomororder'] != "")
					$Detail = $this->Module->Products->detailOrderByNoOrder($_GET['nomororder']);
				else
					$Detail = $this->Module->Products->detailOrder($Id, $this->mUsername);

				$this->Template->assign("Detail", $Detail);

				$this->Template->assign("listRekening", $this->Module->Options->listAdminBank());

				if ($Detail['vKupon'] != "") {
					$Kupon_ = explode("=", $Detail['vKupon']);
					$vKupon = $Kupon_[0];
					$nilaiKupon = $Kupon_[1];
					$this->Template->assign("Kupon", $vKupon);
					$this->Template->assign("nilaiKupon", $nilaiKupon);
				}

				/*
				$Order = $Detail['tOrder_Detail'];
				$Order_ = explode(",", $Order);
				$j=0;
				for ($i=0;$i<count($Order_);$i++)
				{
					$Data_ = explode(":",$Order_[$i]);
					if ($Data_[0]!="")
					{
						if ($Data_[1]!="")
							$detailType = $this->Module->Products->detailProductType($Data_[1]);
						
						$detail = $this->Module->Products->detailItem($Data_[0]);
						$ProductName = ($detail['id']!="")?$detail['vName']:$Data_[3];
						
						if ($detail['id']!="")
						{
							$Data[$j] = array(	'ProductName' => $ProductName, 
												'Qty' => $Data_[2], 
												'Type' => $detailType, 
												'Price' => $Data_[4], 
												'SubTotal' => ($Data_[4]*$Data_[2])
											);
							$j++;
						}
					}
				}
				*/

				$Order = json_decode($Detail['tOrder_Detail'], ARRAY_A);
				$j = 0;
				for ($i = 0; $i < count($Order); $i++) {
					if (is_array($Order[$i])) {
						if ($Order[$i]['id_type'] != "")
							$detailType = $this->Module->Products->detailProductType($Order[$i]['id_type']);

						$Data[$j] = array(
							'id_product' => $Order[$i]['id_product'],
							'ProductName' => $Order[$i]['name_product'],
							'Qty' => $Order[$i]['qty'],
							'Type' => $detailType,
							'Price' => $Order[$i]['price_product'],
							'SubTotal' => ($Order[$i]['price_product'] * $Order[$i]['qty']),
							'Price_Before' => $Order[$i]['price_before'],
							'Discount' => $Order[$i]['discount'],
							'detailProduct' =>  $this->Module->Products->detailItem($Order[$i]['id_product'])
						);
						$j++;
					}
				}

				$subTotal = ($Detail['dTotalPayment'] - $Detail['dShipping']);
				if ($nilaiKupon > 0)
					$subTotal = $subTotal + $nilaiKupon;

				$this->Template->assign("subTotal", $subTotal);
				$this->Template->assign("list", $Data);
				echo $this->Template->show("print_invoice.html");
				break;
			case "detail":
				if ($_GET['nomororder'] != "")
					$Detail = $this->Module->Products->detailOrderByNoOrder($_GET['nomororder'], $this->mUsername);
				else
					$Detail = $this->Module->Products->detailOrder($Id, $this->mUsername);


					$curl = curl_init();
					$subject = 	$Detail[13];
					$pecah=explode("-",$subject);

					$kurir = "jne";
					$waybill = $pecah[1];

					$param = "waybill=".$waybill."&courier=".$kurir;
					
					curl_setopt_array($curl, array(
						CURLOPT_URL => "https://pro.rajaongkir.com/api/waybill",
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 30,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "POST",
						CURLOPT_POSTFIELDS =>$param,
						CURLOPT_HTTPHEADER => array(
								"content-type: application/x-www-form-urlencoded",
								"key: fdf1a35fcd5964690ce7918603132a51"
							),
						));
				
						$response = curl_exec($curl);
						$err = curl_error($curl);
				
						curl_close($curl);
				
				
						$response = json_decode($response);
				
					
				
						$this->Template->assign("detailPengiriman" , $response );
					
				
					
				$this->Template->assign("Detail", $Detail);
				$this->detailMember = $this->Module->Member->detailMember(NULL, $this->mUsername);
				$this->Template->assign("detailMember", $this->detailMember);
			
				echo $this->Template->show("main_order_detail.html");
				break;
			default:

				#code ..
				if (($vCompare == "") or ($vTeks == ""))
					$this->Module->Paging->query("SELECT * FROM cpproductorder WHERE vPembeli='" . $this->mUsername . "' ORDER BY dOrder DESC, id DESC");
				else
					$this->Module->Paging->query("SELECT * FROM cpproductorder WHERE vPembeli='" . $this->mUsername . "' AND " . $vCompare . " LIKE '%" . $vTeks . "%' ORDER BY dOrder DESC, id DESC");

				$this->Module->Paging->URL = $this->getURL() . "memberproduk/transaksipembeli";
				$this->Module->Paging->dataLink = "vCompare=" . $vCompare . "&vTeks=" . $vTeks;
				$page = $this->Module->Paging->print_info();
				$this->Template->assign("status", "Order " . $page[start] . " - " . $page[end] . " of " . $page[total] . " [Total " . $page[total_pages] . " Groups]");
				$i = 0;
				while ($Baca = $this->Module->Paging->result_assoc()) {
					$Data[$i] = array(
						'No' => ($i + 1),
						'Item' => $Baca);
					$i++;
				}

				$this->Template->assign("list", $Data);
				$this->Template->assign("link", $this->Module->Paging->print_link());

				echo $this->Template->show("main_order_index.html");
				break;
		}
	}

	//Katalog
	function katalog()
	{
		$this->LoadModule("Enkripsi");
		$this->Template->assign("t", $this->Module->Enkripsi->encrypt($this->mUsername));

		$this->Template->assign("SIGNATURE", "katalog");
		$this->Pile->fileDestination = $this->Config['upload']['products'];
		$this->Template->assign("dirProducts", $this->Config['upload']['products']);

		//if submit process do-------------------------------
		$Submit = ($_POST['submit']) ? $_POST['submit'] : $_GET['submit'];
		$Id = ($_POST['id']) ? $_POST['id'] : $_GET['id'];

		if ($Submit) {
			//get the ction
			$Action = ($_POST['action']) ? $_POST['action'] : $_GET['action'];

			switch ($Action) {
				case "addslotoption":
				case "updateslotoption":
					$vTitle = $this->Db->filterText($_POST['vTitle']);
					$vPermalink = ($_POST['vPermalink'] == "") ? ereg_replace(" ", "-", strtolower(preg_replace("/[^a-zA-Z0-9\-\s]/", "", $vTitle))) : ereg_replace(" ", "-", strtolower(preg_replace("/[^a-zA-Z0-9\-\s]/", "", $_POST['vPermalink'])));

					$tDesc = $this->Db->filterText($_POST['tDesc']);
					$vMetaTitle = $vTitle;
					$vMetaDesc = substr(strip_tags($tDesc), 0, 250);
					$vMetaKeyword = $vTitle;

					$vPictureName = $this->Pile->simpanImage($_FILES['lbPicture'], "slot" . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . date("Yndhis"));

					if ($Action == "updateslotoption") {
						$detailSlot = $this->Module->Products->detailSlot($Id, $this->mUsername);
						if ($vPictureName != "")
							$this->Pile->deleteOldFile($detailSlot['vPictureName']);
					}

					if (($vTitle != "") and ($vPermalink != "")) {
						if ($Action == "addslotoption") {
							if ($this->Module->Products->addSlot(array($vTitle, $vPermalink, $vPictureName, $tDesc, $vMetaTitle, $vMetaDesc, $vMetaKeyword, $this->mUsername, '', '1', '', '')))
								$this->Template->reportMessage("success", "Katalog produk sudah di tambahkan");
						}

						if ($Action == "updateslotoption") {
							if ($this->Module->Products->updateSlot(array($vTitle, $vPermalink, $vPictureName, $tDesc, $vMetaTitle, $vMetaDesc, $vMetaKeyword), $Id, $this->mUsername))
								$this->Template->reportMessage("success", "Katalog produk ID: " . $Id . " telah di perbaharui");
						}
					}
					break;
				case "deletesloticon":
					$DetailSlot = $this->Module->Products->detailSlot($Id, $this->mUsername);
					$this->Pile->deleteOldFile($DetailSlot['vPictureName']);

					if ($this->Module->Products->deleteSlotIcon($Id, $this->mUsername))
						$this->Template->reportMessage("success", "Icon untuk katalog produk ID: " . $Id . " telah di hapus");
					break;
				case "deleteslotoption":
					if ($Id != "") {
						$this->Module->Products->deleteProductSlotBySlot($Id, $this->mUsername);
						if ($this->Module->Products->deleteSlot($Id, $this->mUsername))
							$this->Template->reportMessage("success", "Katalog produk ID: " . $Id . " telah di hapus");
					}
					break;
					//Product Slot
				case "addslot":
					$idSlot = $_POST['idSlot'];
					$idPrice_ = $_POST['idprice'];
					$idPrice = explode("#", $idPrice_);

					$j = 0;
					for ($i = 0; $i <= count($idPrice); $i++) {
						if (($idPrice[$i] != "") and ($_POST['check_' . $idPrice[$i]] == "yes")) {
							$tData[$j] = $idPrice[$i];
							$j++;
						}
					}

					if ($this->Module->Products->addProductSlot($idSlot, $tData, $this->mUsername))
						$this->Template->reportMessage("success", "Produk telah di tambahkan ke dalam katalog");

					break;
				case "deleteslot":
					if ($this->Module->Products->deleteProductSlot($_GET['slot'], $Id, $this->mUsername))
						$this->Template->reportMessage("success", "Produk telah di hapus dari katalog");
					break;
			}
		}

		$Do = $_GET['do'];
		$this->Template->assign("Do", $Do);

		$Show = $_GET['show'];
		$this->Template->assign("Show", $Show);

		$vCompare = trim($_GET['vCompare']);
		$this->Template->assign("vCompare", $vCompare);

		$vTeks = $this->Db->string_only(trim($_POST['vTeks']));
		$this->Template->assign("vTeks", $vTeks);

		$this->Template->assign("Page", $_GET['page']);

		switch ($this->uri(3)) {
			case "produk":
				$listSlot = $this->Module->Products->listSlot($this->mUsername);
				$this->Template->assign("listSlot", $listSlot);

				$Slot = ($_GET['slot']) ? $_GET['slot'] : $listSlot[0]['Item'][id];
				$this->Template->assign("Slot", $Slot);
				$detailSlot = $this->Module->Products->detailSlot($Slot, $this->mUsername);
				$this->Template->assign("detailSlot", $detailSlot);

				$Cat = $_GET['cat'];
				$this->Template->assign("Cat", $Cat);
				$_Category = ($Cat == "") ? "" : " AND idCategory='" . $Cat . "'";
				$this->Template->assign("listCategory", $this->Module->Products->listBySub(0));
				$productSlot = json_decode($detailSlot['tData']);

				if (($vCompare == "") or ($vTeks == ""))
					$this->Module->Paging->query("SELECT * FROM cpproductitem WHERE id!='0'" . $_Category . " ORDER BY dPublishDate DESC, id DESC");
				else
					$this->Module->Paging->query("SELECT * FROM cpproductitem WHERE id!='0'" . $_Category . " AND " . $vCompare . " LIKE '%" . $vTeks . "%' ORDER BY dPublishDate DESC, id DESC");

				$this->Module->Paging->URL = $this->getURL() . "memberproduk/katalog/produk";
				$this->Module->Paging->dataLink = "slot=" . $Slot . "&cat=" . $Cat . "&vCompare=" . $vCompare . "&vTeks=" . $vTeks;
				$page = $this->Module->Paging->print_info();
				$this->Template->assign("status", "Product " . $page[start] . " - " . $page[end] . " of " . $page[total] . " [Total " . $page[total_pages] . " Groups]");
				$i = 0;
				$idPricee = "";
				while ($Baca = $this->Module->Paging->result_assoc()) {
					$idPricee .= $Baca['id'] . "#";
					$statusSlot = (in_array($Baca['id'], $productSlot)) ? "yes" : "no";
					$getHarga = $this->Module->Products->getAdminUserPrice($Baca['iPrice'], $Baca['iPercent'], "value", $Baca['id']);
					$Data[$i] = array(
						'No' => ($i + 1),
						'Item' => array_merge($Baca, $getHarga),
						'Type' => $this->Module->Products->listProductType($Baca['id'], "all"),
						'Stock' => $Baca['iStock'],
						'Type_List' => $this->Module->Products->listProductType($Baca['id']),
						'Status_Slot' => $statusSlot
					);
					$i++;
				}

				$this->Template->assign("totalProduk", $this->Module->Products->countProductItemTotal($vCompare, $vTeks, "all", "all", $Cat));
				$this->Template->assign("list", $Data);
				$this->Template->assign("link", $this->Module->Paging->print_link());

				$this->Template->assign("idPricee", $idPricee);
				echo $this->Template->Show("main_katalog_addproduk.html");
				break;
			case "list":
				$listSlot = $this->Module->Products->listSlot($this->mUsername);
				$this->Template->assign("listSlot", $listSlot);

				$Slot = ($_GET['slot']) ? $_GET['slot'] : $listSlot[0]['Item'][id];
				$this->Template->assign("Slot", $Slot);

				$hiddenOption .= "<input type=\"hidden\" name=\"action\" value=\"addslot\" />";
				$this->Template->assign("hiddenOption", $hiddenOption);

				$detailSlot = $this->Module->Products->detailSlot($Slot, $this->mUsername);
				$this->Template->assign("detailSlot", $detailSlot);

				$this->Template->assign("Total", $this->Module->Products->countProductSlot($detailSlot['tData']));

				$this->Template->assign("listProductSlot", $this->Module->Products->listProductSlot($detailSlot['tData']));
				echo $this->Template->show("main_katalog_list.html");
				break;
			default:
				switch ($Show) {
					case "edit":
						$Detail = $this->Module->Products->detailSlot($Id, $this->mUsername);
						$hiddenOption = "<input type=hidden name=action value=updateslotoption /><input type=hidden name=id value=" . $Detail['id'] . " />";
						$this->Template->assign("Detail", $Detail);
						break;
					default:
						$hiddenOption = "<input type=hidden name=action value=addslotoption />";
						break;
				}

				$this->Template->assign("hiddenOption", $hiddenOption);

				if (($vCompare == "") or ($vTeks == ""))
					$this->Module->Paging->query("SELECT * FROM cpslot WHERE vUsername='" . $this->mUsername . "' ORDER BY id DESC");
				else
					$this->Module->Paging->query("SELECT * FROM cpslot WHERE vUsername='" . $this->mUsername . "' AND " . $vCompare . " LIKE '%" . $vTeks . "%' ORDER BY id DESC");

				$this->Module->Paging->URL = $this->getURL() . "memberproduk/katalog";
				$this->Module->Paging->dataLink = "vCompare=" . $vCompare . "&vTeks=" . $vTeks;
				$page = $this->Module->Paging->print_info();
				$this->Template->assign("status", "Katalog " . $page[start] . " - " . $page[end] . " of " . $page[total] . " [Total " . $page[total_pages] . " Groups]");
				$i = 0;
				while ($Baca = $this->Module->Paging->result_assoc()) {
					$Data[$i] = array(
						'No' => ($i + 1),	'Item' => $Baca,
						'Count' => $this->Module->Products->countProductSlot($Baca['tData'])
					);
					$i++;
				}
				$this->Template->assign("listSlot", $Data);
				$this->Template->assign("link", $this->Module->Paging->print_link());
				echo $this->Template->Show("main_katalog.html");
				break;
		}
	}

	//Affiliasi
	function affiliasi()
	{
		$this->checkAuthMember(array("all", "all"), $this->detailMember); //Check Auth
		$this->Template->assign("Signature_Sub", "affiliasi");

		$Do = $_GET['do'];
		$this->Template->assign("Do", $Do);

		$Show = $_GET['show'];
		$this->Template->assign("Show", $Show);

		$vCompare = trim($_GET['vCompare']);
		$this->Template->assign("vCompare", $vCompare);

		$vTeks = $this->Db->string_only(trim($_POST['vTeks']));
		$this->Template->assign("vTeks", $vTeks);

		$this->Template->assign("Page", $_GET['page']);

		switch ($Do) {
			default:

				if (($vCompare == "") or ($vTeks == ""))
					$this->Module->Paging->query("SELECT *,(SELECT iStatus FROM cppoinlog WHERE idOrder=a.id) AS pStatus FROM cpproductorder a WHERE a.vAff='" . $this->mUsername . "' ORDER BY a.dOrder DESC, a.id DESC");
				else
					$this->Module->Paging->query("SELECT *,(SELECT iStatus FROM cppoinlog WHERE idOrder=a.id) AS pStatus FROM cpproductorder a WHERE a.vAff='" . $this->mUsername . "' AND a." . $vCompare . " LIKE '%" . $vTeks . "%' ORDER BY a.dOrder DESC, a.id DESC");

				$this->Module->Paging->URL = $this->getURL() . "memberproduk/affiliasi";
				$this->Module->Paging->dataLink = "vCompare=" . $vCompare . "&vTeks=" . $vTeks;
				$page = $this->Module->Paging->print_info();
				$this->Template->assign("status", "Reward " . $page[start] . " - " . $page[end] . " of " . $page[total] . " [Total " . $page[total_pages] . " Groups]");
				$i = 0;
				while ($Baca = $this->Module->Paging->result_assoc()) {
					$Data[$i] = array('No' => ($i + 1),	'Item' => $Baca);
					$i++;
				}

				$this->Template->assign("list", $Data);
				$this->Template->assign("link", $this->Module->Paging->print_link());

				echo $this->Template->show("main_affiliasi_index.html");
				break;
		}
	}

	function linkaffiliasi()
	{
		$this->checkAuthMember(array("all", "all"), $this->detailMember); //Check Auth
		$this->Template->assign("dirProducts", $this->Config['upload']['products']);
		$this->Template->assign("Signature_Sub", "linkaffiliasi");

		$Do = $_GET['do'];
		$this->Template->assign("Do", $Do);

		$Show = $_GET['show'];
		$this->Template->assign("Show", $Show);

		$vCompare = trim($_GET['vCompare']);
		$this->Template->assign("vCompare", $vCompare);

		$vTeks = $this->Db->string_only(trim($_POST['vTeks']));
		$this->Template->assign("vTeks", $vTeks);

		$this->Template->assign("Page", $_GET['page']);

		//Affiliasi Link
		$this->Template->assign("t", $this->Module->Enkripsi->encrypt($this->mUsername));
		//----------------------------------------------------------
		//if ($_GET['p_name']!="")
		//{
		$Data_Aff = $this->Module->Products->detailUserSettingByName("AFFILIASI", "admin");
		$p_Name = $_GET['p_name'];
		$this->Template->assign("p_name", $p_Name);
		$Check_List = ($p_Name == "") ? "" : " AND vName LIKE '%" . $p_Name . "%'";

		$pbaca = $this->Db->sql_query("SELECT * FROM cpproductitem WHERE id!='0'" . $Check_List . " AND iShow='1' ORDER BY id DESC LIMIT 0, 5");
		$j = 0;
		while ($pBaca = $this->Db->sql_array($pbaca)) {
			$getHarga = $this->Module->Products->getAdminUserPrice($pBaca['iPrice'], $pBaca['iPercent'], "value", $pBaca['id']);
			$REWARD_AFFILIASI = round(($Data_Aff['vSetting'] / 100) * $getHarga['feeAdmin']);
			$pData[$j] = array('Item' => $pBaca, 'Affiliasi' => $REWARD_AFFILIASI, 'hargaUser' => $getHarga['sellerPrice']);
			$j++;
		}
		$this->Template->assign("listProduk", $pData);
		//}

		echo $this->Template->show("main_refferal_link.html");
	}

	//Refferal
	function refferal()
	{
		$this->checkAuthMember(array("all", "all"), $this->detailMember); //Check Auth
		$this->Template->assign("Signature_Sub", "affiliasi");

		$Do = $_GET['do'];
		$this->Template->assign("Do", $Do);

		$Show = $_GET['show'];
		$this->Template->assign("Show", $Show);

		$vCompare = trim($_GET['vCompare']);
		$this->Template->assign("vCompare", $vCompare);

		$vTeks = $this->Db->string_only(trim($_POST['vTeks']));
		$this->Template->assign("vTeks", $vTeks);

		$this->Template->assign("Page", $_GET['page']);

		switch ($Do) {
			default:

				if (($vCompare == "") or ($vTeks == ""))
					$this->Module->Paging->query("SELECT *,(SELECT iStatus FROM cprefflog WHERE idOrder=a.id) AS pStatus FROM cpproductorder a WHERE a.vAff='" . $this->mUsername . "' ORDER BY a.dOrder DESC, a.id DESC");
				else
					$this->Module->Paging->query("SELECT *,(SELECT iStatus FROM cprefflog WHERE idOrder=a.id) AS pStatus FROM cpproductorder a WHERE a.vAff='" . $this->mUsername . "' AND a." . $vCompare . " LIKE '%" . $vTeks . "%' ORDER BY a.dOrder DESC, a.id DESC");

				$this->Module->Paging->URL = $this->getURL() . "memberproduk/refferal";
				$this->Module->Paging->dataLink = "vCompare=" . $vCompare . "&vTeks=" . $vTeks;
				$page = $this->Module->Paging->print_info();
				$this->Template->assign("status", "Reward " . $page[start] . " - " . $page[end] . " of " . $page[total] . " [Total " . $page[total_pages] . " Groups]");
				$i = 0;
				while ($Baca = $this->Module->Paging->result_assoc()) {
					$Data[$i] = array('No' => ($i + 1),	'Item' => $Baca);
					$i++;
				}

				$this->Template->assign("list", $Data);
				$this->Template->assign("link", $this->Module->Paging->print_link());

				echo $this->Template->show("main_refferal_index.html");
				break;
		}
	}

	//Poin Daftar
	function poindaftar()
	{
		$this->checkAuthMember(array("all", "all"), $this->detailMember); //Check Auth
		$this->Template->assign("Signature_Sub", "poindaftar");

		$Do = $_GET['do'];
		$this->Template->assign("Do", $Do);

		$Show = $_GET['show'];
		$this->Template->assign("Show", $Show);

		$vCompare = trim($_GET['vCompare']);
		$this->Template->assign("vCompare", $vCompare);

		$vTeks = $this->Db->string_only(trim($_POST['vTeks']));
		$this->Template->assign("vTeks", $vTeks);

		$this->Template->assign("Page", $_GET['page']);

		switch ($Do) {
			default:

				if (($vCompare == "") or ($vTeks == ""))
					$this->Module->Paging->query("SELECT * cppoinlog WHERE vUsername='" . $this->mUsername . "' AND vAff!='' AND (idOrder='0' OR idOrder='') ORDER BY id DESC");
				else
					$this->Module->Paging->query("SELECT * cppoinlog WHERE vUsername='" . $this->mUsername . "' AND vAff!='' AND (idOrder='0' OR idOrder='') ORDER BY id DESC");

				$this->Module->Paging->URL = $this->getURL() . "memberproduk/poindaftar";
				$this->Module->Paging->dataLink = "vCompare=" . $vCompare . "&vTeks=" . $vTeks;
				$page = $this->Module->Paging->print_info();
				$this->Template->assign("status", "Reward " . $page[start] . " - " . $page[end] . " of " . $page[total] . " [Total " . $page[total_pages] . " Groups]");
				$i = 0;
				while ($Baca = $this->Module->Paging->result_assoc()) {
					$Data[$i] = array(
						'No' => ($i + 1),
						'Item' => $Baca,
						'Daftar' => $this->Module->Member->detailMember(NULL, $Baca['vAff'])
					);
					$i++;
				}

				$this->Template->assign("list", $Data);
				$this->Template->assign("link", $this->Module->Paging->print_link());

				echo $this->Template->show("main_poindaftar.html");
				break;
		}
	}

	function linkdaftar()
	{
		$this->checkAuthMember(array("all", "all"), $this->detailMember); //Check Auth
		$this->Template->assign("dirProducts", $this->Config['upload']['products']);
		$this->Template->assign("Signature_Sub", "linkaffiliasi");

		$Do = $_GET['do'];
		$this->Template->assign("Do", $Do);

		$Show = $_GET['show'];
		$this->Template->assign("Show", $Show);

		$vCompare = trim($_GET['vCompare']);
		$this->Template->assign("vCompare", $vCompare);

		$vTeks = $this->Db->string_only(trim($_POST['vTeks']));
		$this->Template->assign("vTeks", $vTeks);

		$this->Template->assign("Page", $_GET['page']);

		//Affiliasi Link
		$this->Template->assign("t", $this->Module->Enkripsi->encrypt($this->mUsername));
		//----------------------------------------------------------
		//if ($_GET['p_name']!="")
		//{
		$Data_Aff = $this->Module->Products->detailUserSettingByName("AFFILIASI", "admin");
		$p_Name = $_GET['p_name'];
		$this->Template->assign("p_name", $p_Name);
		$Check_List = ($p_Name == "") ? "" : " AND vName LIKE '%" . $p_Name . "%'";

		$pbaca = $this->Db->sql_query("SELECT * FROM cpproductitem WHERE id!='0'" . $Check_List . " AND iShow='1' ORDER BY id DESC LIMIT 0, 5");
		$j = 0;
		while ($pBaca = $this->Db->sql_array($pbaca)) {
			$getHarga = $this->Module->Products->getAdminUserPrice($pBaca['iPrice'], $pBaca['iPercent'], "value", $pBaca['id']);
			$REWARD_AFFILIASI = round(($Data_Aff['vSetting'] / 100) * $getHarga['feeAdmin']);
			$pData[$j] = array('Item' => $pBaca, 'Affiliasi' => $REWARD_AFFILIASI, 'hargaUser' => $getHarga['sellerPrice']);
			$j++;
		}
		$this->Template->assign("listProduk", $pData);
		//}

		echo $this->Template->show("main_linkdaftar.html");
	}
}
